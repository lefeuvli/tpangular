import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cofondateur'
})
export class CofondateurPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var txtReturn = ""
    if (value == 1) {
      txtReturn = "Unique"
    } else if (value == 2) {
      txtReturn = "Couple"
    } else if (value > 2) {
      txtReturn = "Groupes"
    }
    return txtReturn;
  }

}
