import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartupComponent } from './components/startup/startup.component';
import { ConsultantComponent } from './components/consultant/consultant.component';
import { HomeComponent } from './components/home/home.component';
import { Page404Component } from './page404/page404.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'startups', component: StartupComponent },
  { path: 'consultants', component: ConsultantComponent },
  { path: 'page404', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
