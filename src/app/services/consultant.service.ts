import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Consultant } from '../model';

@Injectable({
  providedIn: 'root'
})
export class ConsultantService {

  constructor(private http: HttpClient) { }

  get_all(): Observable<Consultant[]> {
    return this.http.get<Consultant[]>("${baseUrl}/consultants")
  }

  get(id): Observable<Consultant> {
    return this.http.get<Consultant>("${baseUrl}/consultants/" + id)
  }
}
