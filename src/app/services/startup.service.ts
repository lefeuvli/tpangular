import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Startup } from '../model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  constructor(private http: HttpClient) { }

  get_all(): Observable<Startup[]> {
    return this.http.get<Startup[]>("${baseUrl}/startups");
  }

  get(id): Observable<Startup> {
    return this.http.get<Startup>("${baseUrl}/startups/" + id);
  }

  add(startup) {
    return this.http.post<Startup>("${baseUrl}/startups/", startup);
  }

  edit(startup) {
    return this.http.put<Startup>("${baseUrl}/startups/" + startup.id, startup);
  }

  delete(startup) {
    return this.http.delete<Startup>("${baseUrl}/startups/" + startup);
  }
}
