import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Startup, Consultant } from '../model';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  constructor() { }

  createDb() {
    const consultants = [
      new Consultant(1, "NConsultant1", "PConsultant1", "Description1"),
      new Consultant(2, "NConsultant2", "PConsultant2", "Description2"),
      new Consultant(3, "NConsultant3", "PConsultant3", "Description3"),
      new Consultant(3, "NConsultant4", "PConsultant4", "Description4")
    ];

    const startups = [
      new Startup(1, "Startup1", "Secteur1", "Representant1", 1, "description1", consultants[0]),
      new Startup(2, "Startup2", "Secteur2", "Representant2", 2, "description2", consultants[1], "adresse"),
      new Startup(3, "Startup3", "Secteur3", "Representant3", 1, "description3", consultants[2]),
      new Startup(4, "Startup4", "Secteur4", "Representant4", 3, "description4", consultants[1], "adresse")
    ];

    return { startups, consultants}
  }

  genId<T extends Startup | Consultant>(table: T[]): number {
    return table.length > 0 ? Math.max(...table.map(t => t.id)) + 1 : 11;
  }
}
