import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './services/in-memory-data.service';
import { StartupComponent } from './components/startup/startup.component';
import { ConsultantComponent } from './components/consultant/consultant.component';
import { CofondateurPipe } from './pipes/cofondateur.pipe';
import { HomeComponent } from './components/home/home.component';
import { CustomCardComponent } from './components/custom-card/custom-card.component';
import { Page404Component } from './page404/page404.component';
import { httpInterceptorProviders } from './http-interceptors';

@NgModule({
  declarations: [
    AppComponent,
    StartupComponent,
    ConsultantComponent,
    CofondateurPipe,
    HomeComponent,
    CustomCardComponent,
    Page404Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
