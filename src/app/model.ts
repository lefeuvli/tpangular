export class Startup {
    id: number;
    nom: string;
    secteur: string;
    representant: string;
    nbCofondateur: number;
    description: string;
    adresse: string;
    consultant: Consultant

    constructor(id: number, nom: string, secteur: string, representant: string, nbCofondateur: number, description: string, consultant: Consultant, adresse: string= "") {
        this.id = id;
        this.nom = nom;
        this.secteur = secteur;
        this.representant = representant;
        this.nbCofondateur = nbCofondateur;
        this.description = description;
        this.consultant = consultant;
        this.adresse = adresse
    }
}

export class Consultant {
    id: number;
    nom: string;
    prenom: string;
    description: string;

    constructor(id: number, nom: string, prenom: string, description: string) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.description = description;
    }
}