import { Component, OnInit } from '@angular/core';
import { Startup, Consultant } from 'src/app/model';
import { StartupService } from 'src/app/services/startup.service';
import { ConsultantService } from 'src/app/services/consultant.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  tabStartup: Startup[];
  tabConsultant: Consultant[];

  constructor(private startupService: StartupService, private consultantService: ConsultantService) { }

  ngOnInit() {
    this.startupService.get_all().subscribe(
      tab => this.tabStartup = tab
    )
    this.consultantService.get_all().subscribe(
      tab => this.tabConsultant = tab
    )
  }

}
