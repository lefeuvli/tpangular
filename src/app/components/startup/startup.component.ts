import { Component, OnInit } from '@angular/core';
import { Startup, Consultant } from 'src/app/model';
import { StartupService } from 'src/app/services/startup.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ConsultantService } from 'src/app/services/consultant.service';

@Component({
  selector: 'app-startup',
  templateUrl: './startup.component.html',
  styleUrls: ['./startup.component.css']
})
export class StartupComponent implements OnInit {
  tabStartup: Startup[];
  displayForm: boolean = false;

  startupForm: FormGroup;
  idCtrl: FormControl;
  nomCtrl: FormControl;
  secteurCtrl: FormControl;
  representantCtrl: FormControl;
  nbCofondateurCtrl: FormControl;
  descriptionCtrl: FormControl;
  adresseCtrl: FormControl;
  consultantCtrl: FormControl;

  constructor(private startupService: StartupService, private consultantService: ConsultantService, public fb: FormBuilder) {
    this.idCtrl = fb.control('');
    this.nomCtrl = fb.control('', [Validators.required, Validators.maxLength(20)]);
    this.secteurCtrl = fb.control('', [Validators.required, Validators.maxLength(10)]);
    this.representantCtrl = fb.control('', [Validators.required, Validators.maxLength(15)]);
    this.nbCofondateurCtrl = fb.control('', [Validators.required, Validators.pattern("[0-9]*")]);
    this.descriptionCtrl = fb.control('', [Validators.required, Validators.maxLength(250)]);
    this.adresseCtrl = fb.control('', [Validators.maxLength(25)]);
    this.consultantCtrl = fb.control('');
    this.startupForm = fb.group({
      id: this.idCtrl,
      nom: this.nomCtrl,
      secteur: this.secteurCtrl,
      representant: this.representantCtrl,
      nbCofondateur: this.nbCofondateurCtrl,
      description: this.descriptionCtrl,
      adresse: this.adresseCtrl,
      consultant: this.consultantCtrl
    })
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.startupService.get_all().subscribe(
      tab => this.tabStartup = tab
    )
  }

  voirForm() {
    this.displayForm = !this.displayForm;
    this.startupForm.reset();
  }

  ajouter() {
    if (this.startupForm.value.id) {
      this.startupService.edit(this.startupForm.value).subscribe(
        result => this.tabStartup.map(startup => {
          if (startup.id == this.startupForm.value.id) {
            startup = this.startupForm.value
          }
        })
      )
    } else {
      this.consultantService.get_all().subscribe({
        next: data => {
          var startup = this.startupForm.value;
          var nbRand = Math.floor(Math.random() * data.length);
          startup.consultant = data[nbRand];
          this.startupService.add(startup).subscribe(
            result => this.addStartup(result)
          )
        }
      })
    }
    this.displayForm = false;
    this.getData();
  }

  addStartup(startup) {
    this.tabStartup.push(startup);
    this.startupForm.reset();
  }

  voirModif(id) {
    this.displayForm = true;
    this.startupService.get(id).subscribe(
      x => this.startupForm.setValue(x)
    )
  }

  supprimer(id) {
    this.startupService.delete(id).subscribe(
      result => this.tabStartup.filter(e => e.id != id)
    )
    this.getData();
  }

}
