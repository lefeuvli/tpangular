import { Component, OnInit } from '@angular/core';
import { Consultant } from 'src/app/model';
import { ConsultantService } from 'src/app/services/consultant.service';

@Component({
  selector: 'app-consultant',
  templateUrl: './consultant.component.html',
  styleUrls: ['./consultant.component.css']
})
export class ConsultantComponent implements OnInit {
  tabConsultant: Consultant[]

  constructor(private consultantService: ConsultantService) { }

  ngOnInit() {
    this.consultantService.get_all().subscribe(
      tab => this.tabConsultant = tab
    )
  }

}
